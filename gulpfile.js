var gulp = require('gulp');
var browsersync = require('browser-sync').create();
var sass = require('gulp-sass');
var pug = require('gulp-pug');
var bourbon = require('node-bourbon').includePaths;
var normalize = require('node-normalize-scss').includePaths;
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var jshint = require('gulp-jshint');
var uglify = require('gulp-uglify');
var pump = require('pump');
var rename = require('gulp-rename');

// Compile pug into HTML
gulp.task('pug', function(){
  return gulp.src("src/pug/*.pug")
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(pug({
      pretty: true
    }))
    .pipe(gulp.dest("dist/"))
    .pipe(browsersync.stream());
});

// Compile SASS into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src("src/scss/*.scss")
    .pipe(plumber({errorHandler: notify.onError("Error: <%= error.message %>")}))
    .pipe(sass({
      includePaths: [].concat(normalize, bourbon),
      outputStyle: 'compressed'
    }))
    .pipe(gulp.dest("dist/assets/css/"))
    .pipe(browsersync.stream());
});

// Lint JS files
gulp.task('lint', function() {
  return gulp.src('src/js/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});

// Concat JS with sourcemaps
gulp.task('javascript', function() {
  return gulp.src([
    'node_modules/jquery/dist/jquery.min.js',
    'src/js/*.js'
  ])
    .pipe(sourcemaps.init())
      .pipe(concat('scripts.js'))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('dist/assets/js'));
});

// Static Server + watching scss/html files
gulp.task('serve', ['pug', 'sass', 'lint', 'javascript'], function() {

  browsersync.init({
      server: {
        baseDir: 'dist'
      }
  });

  gulp.watch("src/pug/**/*.pug", ['pug']);
  gulp.watch("src/scss/**/*.scss", ['sass']);
  gulp.watch("src/js/*.js", ['javascript']);
});

gulp.task('default', ['serve']);

// Minify JS
// gulp.task('compress', function (cb) {
//   pump([
//       gulp.src('dist/assets/js/scripts.js'),
//       uglify(),
//       rename({ suffix: '.min' }),
//       gulp.dest('dist/assets/js/')
//     ],
//     cb
//   );
// });

